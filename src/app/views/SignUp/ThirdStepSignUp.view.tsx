import React, { Component, Fragment } from 'react';
import { View, Button, Text, TextInput, Alert, ActivityIndicator, ScrollView } from 'react-native';
import styles from './styles';
import ValidationService from '../../services/validation.service';
import axios from 'axios';

interface State {
    firstName: string,
    lastName: string,
    phoneNumber: string,
    phoneError: boolean,
    isLoading: boolean
}

export default class ThirdStepSignUp extends Component<any, State> {
    private validationservice = new ValidationService();
    
    constructor (props: any) {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            phoneNumber: '',
            phoneError: false,
            isLoading: false
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        const firstName = JSON.stringify(navigation.getParam('firstName', 'John'));
        const lastName = JSON.stringify(navigation.getParam('lastName', 'Doe'));
        // const phoneNumber = JSON.stringify(navigation.getParam('phoneNumber', '9379992'));
        
        this.setState({
            firstName: firstName,
            lastName: lastName,
            // phoneNumber: phoneNumber,
        });
    }

    private handleChange = async (data: any): Promise<any> => {
        this.setState({
            phoneNumber: data,
            phoneError: false
        });
    }

    private register = () => {
        const { firstName, lastName, phoneNumber } = this.state;
        const validationError = this.validationservice.validatePhoneNumber(phoneNumber);

        if(!validationError || !phoneNumber) {
            this.setState({
                phoneError: true
            })
            return;
        }

        this.setState({
            isLoading: true
        })
        // for loading delay
        setTimeout(() => {
            this.setState({
                isLoading: false
            })
        }, 3000)

        axios({
            method: 'post',
            url: '/user/12345',
            data: {
              firstName: firstName,
              lastName: lastName,
              phoneNumer: phoneNumber
            }
        });

        Alert.alert(
            'Congratulations',
            'You succesfully passed registration',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false },
          );
    }

    render() {
        const { phoneNumber, phoneError, isLoading } = this.state;
        return(
            <Fragment>
                <ScrollView>
                    <View style={styles.container}>
                        { isLoading ? 
                        <View style={ styles.loader }>
                            <ActivityIndicator size="large" color="#ffffff" />
                        </View> 
                        : null }
                        <Text style={ styles.formTitle }>SignUp (3rd step)</Text>

                        {/* Phone Number */}
                        <Text style={ styles.label }>Please paste below copied phone number from the previous step</Text>
                        <TextInput 
                            style={ styles.input } 
                            placeholder={ 'Phone Number' }
                            value={ phoneNumber }
                            onChangeText={ (text) => { this.handleChange({ text }) }}
                        ></TextInput>
                        { phoneError ? <Text style={ styles.error }>Input should not be empty</Text> : null }

                        <View style={ styles.buttonContainer }>
                            <Button
                                title="Register"
                                onPress={() => { this.register() }}
                            />
                        </View>
                    </View>
                </ScrollView>
            </Fragment>
        )
    }
}