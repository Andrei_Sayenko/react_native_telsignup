import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        padding: 20,
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    input: {
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        width: '90%',
        margin: 5,
        backgroundColor: '#ffffff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    inputWithErrors: {
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: 'red',
        backgroundColor: 'rgba(255, 0, 0, 0.15)',
        width: '90%',
        margin: 5,
    },
    label: {
        width: '90%', 
    }, 
    formTitle: {
        marginBottom: 20,
        fontSize: 24,
        fontWeight: '600'
    },
    error: {
        color: 'red'
    },
    loader: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.4)',
        zIndex: 1000
    },
    buttonContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        flexDirection: 'row',
        width: '90%',
        maxHeight: 100,
        marginTop: 20
    }
});