import React, { Component, Fragment } from 'react';
import { View, Button, Text, TextInput, Clipboard, ScrollView } from 'react-native';
import styles from './styles';
import CryptoJS  from 'crypto-js';
import environment from '../../environments/environment';

export default class SecondStepSignUp extends Component<any, any> {
    constructor (props: any) {
        super(props);

        this.state = {
            phoneNumber: '',
            textWasCopied: false,
            allowedGoForward: false
        }
    }
    
    componentDidMount() {
        const { navigation } = this.props;
        const phoneNumber = JSON.stringify(navigation.getParam('phoneNumber', '9379992'));
        const phoneNumberEncrypted = CryptoJS.AES.encrypt(phoneNumber, environment.encryptionKey).toString();
        this.setState({
            phoneNumber: phoneNumberEncrypted,
        });
    }

    private copyToClipboard = (phoneNumber: any) => {
        Clipboard.setString(phoneNumber);
        this.setState({
            textWasCopied: true,
            allowedGoForward: true
        })
        setTimeout(() => {
            this.setState({
                textWasCopied: false
            });
        }, 4000)
    }

    private nextStep = () => {
        const { firstName, lastName, phoneNumber } = this.state;
        
        this.props.navigation.navigate('ThirdStepSignUp', {
            firstName: firstName,
            lastName: lastName,
            phoneNumber: phoneNumber,
        });
    }

    render() {
        const { phoneNumber, textWasCopied, allowedGoForward } = this.state;
        return(
            <Fragment>
                <ScrollView>
                    <View style={styles.container}>
                        <Text style={ styles.formTitle }>SignUp (2nd step)</Text>

                        {/* Phone Number */}
                        <Text style={ styles.label }>Encrypted Phone Number (copy code below)</Text>
                        
                        <TextInput 
                            style={ styles.input } 
                            placeholder={ 'Phone Number' }
                            value={ phoneNumber }
                            // editable={ false }
                            onFocus={() => { this.copyToClipboard(phoneNumber) }}
                        ></TextInput>
                        { textWasCopied ? <Text>Encrypted phone number was copied</Text> : null}
                        <View style={ styles.buttonContainer }>
                            <Button
                                title="Next"
                                disabled={ !allowedGoForward }
                                onPress={() => { this.nextStep() }}
                            />
                        </View>
                    </View>
                </ScrollView>
            </Fragment>
        )
    }
}