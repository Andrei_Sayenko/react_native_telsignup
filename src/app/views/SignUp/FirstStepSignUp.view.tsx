import React, { Component, Fragment } from 'react';
import { View, Button, Text, TextInput, ScrollView } from 'react-native';
import DeviceInfo from 'react-native-device-info';

import ValidationService from '../../services/validation.service';

// import RNSimData from 'react-native-sim-data';
import styles from './styles';
// Permissions
import { requestPhoneStatePermission, requestContactsPermission, requestCallLogPermission  } from '../../helpers/permissions.helper';

interface State {
    firstName: string,
    lastName: string,
    phoneNumber: string,
    errors: {
        firstName: boolean,
        lastName: boolean,
        phoneNumber: boolean,
        formError: boolean
    }
}

// interface Props {
//     navigate: () => {
        
//     }
// }

export default class FirstStepSignUp extends Component<any, State> {
    private validationService = new ValidationService();

    constructor(props: any) {
        super(props);
        this.state= {
            firstName: '',
            lastName: '',
            phoneNumber: '',
            errors: {
                firstName: false,
                lastName: false,
                phoneNumber: false,
                formError: false             
            }
        }
    }

    componentDidMount() {
        this.getSimData();
        this.getPermissions();
    }

    private getPermissions = (): void => {
        requestPhoneStatePermission();
        requestContactsPermission();
        requestCallLogPermission();
    }

    private getSimData = async (): Promise<any> => {
        const phoneNumber = await DeviceInfo.getPhoneNumber();

        if (phoneNumber) {
            this.setState({
                phoneNumber: phoneNumber
            });
        }
    }

    private handleChange = async (data: any): Promise<any> => {
        let key = Object.keys(data)[0];
        let newValidationObject = this.state.errors;
        (newValidationObject as any)[key] = false;

        this.setState({
            ...data,
            errors: {
                ...newValidationObject,
            }
        });
    }

    private nextStep = async (): Promise<any> => {
        const { firstName, lastName, phoneNumber } = this.state;

        const validationError = this.validationService.validateForm(firstName, lastName, phoneNumber);

        await this.setState(prevState => ({
            errors: {
                ...prevState.errors,
                firstName: validationError.firstName,
                lastName: validationError.lastName,
                phoneNumber: validationError.phoneNumber,
                formError: validationError.formError,
            }
        }));

        if (this.state.errors.formError) { return }
        
        this.props.navigation.navigate('SecondStepSignUp', {
            firstName: firstName,
            lastName: lastName,
            phoneNumber: phoneNumber,
        });
    }

    render() {
      const { firstName, lastName, phoneNumber, errors } = this.state;

      return ( 
        <Fragment>
            <ScrollView>
                <View style={styles.container}>
                    <Text style={ styles.formTitle }>SignUp (1st step)</Text>

                    {/* Fisrt Name */}
                    <Text style={ styles.label }>First Name</Text>
                    <TextInput 
                        style={ errors.firstName? styles.inputWithErrors : styles.input } 
                        placeholder={ 'First Name' }                
                        onChangeText={(firstName) => this.handleChange({ firstName })}
                        value={ firstName }
                        keyboardType={ 'default' }
                    ></TextInput>
                    { errors.firstName ? <Text style={ styles.error }>First Name can contain only letters</Text> : null }

                    {/* Last Name */}
                    <Text style={ styles.label }>Last Name</Text>
                    <TextInput 
                        style={ errors.lastName? styles.inputWithErrors : styles.input } 
                        placeholder={ 'Last Name' }
                        onChangeText={(lastName) => this.handleChange({ lastName })}
                        value={ lastName }
                        keyboardType={ 'default' }
                    ></TextInput>
                    { errors.lastName ? <Text style={ styles.error }>Last Name can contain only letters</Text> : null }

                    {/* Phone Number */}
                    <Text style={ styles.label }>Phone Number</Text>
                    <TextInput 
                        style={ errors.phoneNumber? styles.inputWithErrors : styles.input } 
                        placeholder={ 'Phone Number' }
                        onChangeText={(phoneNumber) => this.handleChange({ phoneNumber })}
                        value={ phoneNumber }
                        keyboardType={ 'phone-pad' }
                    ></TextInput>
                    { errors.phoneNumber ? <Text style={ styles.error }>Unappropriate format of Phone number</Text> : null }
                    <View style={ styles.buttonContainer }>
                        <Button
                            title="Next"
                            onPress={() => { this.nextStep() }}
                        />
                    </View>
                </View>
            </ScrollView>
        </Fragment>
        );
    }
}
