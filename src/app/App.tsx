import React, { Component } from 'react';
import AppContainer from './navigation/app.routing';

export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}
