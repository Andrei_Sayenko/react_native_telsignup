import * as regExps from './regexps';

export default class ValidationService {

    public validatePhoneNumber(number: string): boolean {
        return regExps.nonEmptyRegExp.test(number);
    }

    public validateForm(firstName: string, lastName: string, phoneNumber: string) {
        const validationObject: any = {
            firstName: !regExps.nameRegExp.test(firstName),
            lastName: !regExps.nameRegExp.test(lastName),
            phoneNumber: !regExps.phoneNumberRegExp.test(phoneNumber),
            formError: false,
        }


        for (let key in validationObject) {
            validationObject.formError = validationObject.formError || validationObject[key];
        }

        return validationObject;
    }

}
