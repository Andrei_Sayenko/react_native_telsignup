export const nameRegExp = /^[a-z ,.'-]+$/i;
export const phoneNumberRegExp = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
export const nonEmptyRegExp = /./;

export const formValidators ={
    firstname: nameRegExp,
    lastName: nameRegExp,
    phoneNumber: phoneNumberRegExp,
}
