import { PermissionsAndroid } from 'react-native';

export const requestPhoneStatePermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
      {
        title: 'TelSignUp Read Phone State Permission',
        message:
          'TelSignUp App needs access to your phone state ' +
          'register you in app system.',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('You can use your phone data');
    } else {
      console.log('Phone State permission denied');
    }
  } catch (err) {
    console.warn(err);
  }
}

export const requestContactsPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        {
          title: 'TelSignUp Read Contacts Permission',
          message:
            'TelSignUp App needs access to your Contacts',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Read Contacts permission accepted');
      } else {
        console.log('Read Contacts permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
}

export const requestCallLogPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CALL_LOG,
        {
          title: 'TelSignUp App Call Log Permission',
          message:
            'TelSignUp App needs access to your Call Log',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Call Log permission accepted');
      } else {
        console.log('Call Log permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
}