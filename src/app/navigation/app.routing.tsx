import {createAppContainer, createSwitchNavigator} from 'react-navigation';
// import {createStackNavigator} from 'react-navigation-stack';

// Views
import FirstStepSignUp from '../views/SignUp/FirstStepSignUp.view';
import SecondStepSignUp from '../views/SignUp/SecondStepSignUp.view';
import ThirdStepSignUp from '../views/SignUp/ThirdStepSignUp.view';

const AppNavigator = createSwitchNavigator({
    FirstStepSignUp: {
        screen: FirstStepSignUp,
    },
    SecondStepSignUp: {
        screen: SecondStepSignUp
    },
    ThirdStepSignUp: {
        screen: ThirdStepSignUp
    }
});

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;

